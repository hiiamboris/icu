Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/es_MX: #(
    region-name: "México"
    currency: MXN
    calendar: #(
        day1: sun
        standalone: #(
            days: #(
                char: #(
                    wed: "M"
                )
            )
            quarters: #(
                char: ["1T" "2T" "3T" "4T"]
            )
        )
        format: #(
            days: #(
                char: #(
                    wed: "M"
                )
            )
            quarters: #(
                char: ["1T" "2T" "3T" "4T"]
            )
        )
        masks: #(
            date: #(
                short: "031/012/99"
            )
            time: #(
                full: "023:59:59 'GMT'+00:00"
                long: "023:59:59 'GMT'+0"
                medium: "023:59:59"
                short: "023:59"
            )
            datetime: #(
                full: {Sunday, 31 'de' December 'de' 1999, 023:59:59 'GMT'+00:00}
                long: "31 'de' December 'de' 1999, 023:59:59 'GMT'+0"
                medium: "31 Dec 1999 023:59:59"
                short: "031/012/99, 023:59"
            )
        )
    )
    currency-names: #(
        BYN: [char: "p."]
        MRO: [std: "MRU"]
        MRU: [std: "UM"]
        MXN: [std: "$"]
        RON: [char: "lei"]
    )
    parent: es
)
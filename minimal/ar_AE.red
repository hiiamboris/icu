Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/ar_AE: #(
    region-name: "الإمارات العربية المتحدة"
    currency: AED
    numbers: #(
        system: latn
        latn: #(
            digits: "0123456789"
            fin-digits: "0123456789"
            symbols: #(
                nan: "ليس رقمًا"
                infinity: "∞"
                permille: "‰"
                superscripting-exponent: "×"
                exponential: "E"
                approximately: "~"
                minus: "‎-"
                plus: "‎+"
                percent: "‎%‎"
                list: ";"
                group: ","
                decimal: "."
            )
            masks: #(
                number: #(
                    dec: "# ##0.###"
                    sci: "0.##############E0"
                    pct: "# ##0.%"
                    eng: "0.##############E3"
                )
                money: #(
                    std: "$$ # ##0.00"
                    acct: "($$# ##0.00)"
                )
            )
        )
    )
    calendar: #(
        day1: sat
        format: #(
            eras: #(
                full: #(
                    BCE: "قبل الميلاد"
                )
                abbr: #(
                    BCE: "ق.م"
                )
            )
        )
    )
    currency-names: #(
        BND: [char: "$"]
    )
    parent: ar
)
Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/ar: #(
    lang-name: "العربية"
    region-name: none
    currency: SAR
    numbers: #(
        system: arab
        arab: #(
            digits: "٠١٢٣٤٥٦٧٨٩"
            fin-digits: "٠١٢٣٤٥٦٧٨٩"
            symbols: #(
                nan: "ليس رقم"
                infinity: "∞"
                permille: "؉"
                superscripting-exponent: "×"
                exponential: "اس"
                approximately: "~"
                minus: "؜-"
                plus: "؜+"
                percent: "٪؜"
                list: "؛"
                group: "٬"
                decimal: "٫"
            )
            masks: #(
                number: #(
                    dec: "# ##0.###"
                    sci: "0.##############E0"
                    pct: "# ##0.%"
                    eng: "0.##############E3"
                )
                money: #(
                    std: "# ##0.00 $$"
                    acct: "# ##0.00 $$"
                )
            )
        )
        ordinal-suffixes: #(
            other: " يمينًا."
        )
    )
    calendar: #(
        standalone: #(
            months: #(
                abbr: ["يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر"]
                char: ["ي" "ف" "م" "أ" "و" "ن" "ل" "غ" "س" "ك" "ب" "د"]
                full: ["يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر"]
            )
            days: #(
                abbr: #(
                    sun: "الأحد"
                    mon: "الاثنين"
                    tue: "الثلاثاء"
                    wed: "الأربعاء"
                    thu: "الخميس"
                    fri: "الجمعة"
                    sat: "السبت"
                )
                char: #(
                    sun: "ح"
                    mon: "ن"
                    tue: "ث"
                    wed: "ر"
                    thu: "خ"
                    fri: "ج"
                    sat: "س"
                )
                short: #(
                    sun: "أحد"
                    mon: "إثنين"
                    tue: "ثلاثاء"
                    wed: "أربعاء"
                    thu: "خميس"
                    fri: "جمعة"
                    sat: "سبت"
                )
                full: #(
                    sun: "الأحد"
                    mon: "الاثنين"
                    tue: "الثلاثاء"
                    wed: "الأربعاء"
                    thu: "الخميس"
                    fri: "الجمعة"
                    sat: "السبت"
                )
            )
            quarters: #(
                abbr: ["الربع الأول" "الربع الثاني" "الربع الثالث" "الربع الرابع"]
                char: ["١" "٢" "٣" "٤"]
                full: ["الربع الأول" "الربع الثاني" "الربع الثالث" "الربع الرابع"]
            )
            periods: #(
                abbr: #(
                    am: "ص"
                    pm: "م"
                )
                char: #(
                    am: "ص"
                    pm: "م"
                )
                full: #(
                    am: "صباحًا"
                    pm: "مساءً"
                )
            )
        )
        format: #(
            months: #(
                abbr: ["يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر"]
                char: ["ي" "ف" "م" "أ" "و" "ن" "ل" "غ" "س" "ك" "ب" "د"]
                full: ["يناير" "فبراير" "مارس" "أبريل" "مايو" "يونيو" "يوليو" "أغسطس" "سبتمبر" "أكتوبر" "نوفمبر" "ديسمبر"]
            )
            days: #(
                abbr: #(
                    sun: "الأحد"
                    mon: "الاثنين"
                    tue: "الثلاثاء"
                    wed: "الأربعاء"
                    thu: "الخميس"
                    fri: "الجمعة"
                    sat: "السبت"
                )
                char: #(
                    sun: "ح"
                    mon: "ن"
                    tue: "ث"
                    wed: "ر"
                    thu: "خ"
                    fri: "ج"
                    sat: "س"
                )
                short: #(
                    sun: "أحد"
                    mon: "إثنين"
                    tue: "ثلاثاء"
                    wed: "أربعاء"
                    thu: "خميس"
                    fri: "جمعة"
                    sat: "سبت"
                )
                full: #(
                    sun: "الأحد"
                    mon: "الاثنين"
                    tue: "الثلاثاء"
                    wed: "الأربعاء"
                    thu: "الخميس"
                    fri: "الجمعة"
                    sat: "السبت"
                )
            )
            quarters: #(
                abbr: ["الربع الأول" "الربع الثاني" "الربع الثالث" "الربع الرابع"]
                char: ["١" "٢" "٣" "٤"]
                full: ["الربع الأول" "الربع الثاني" "الربع الثالث" "الربع الرابع"]
            )
            periods: #(
                abbr: #(
                    am: "ص"
                    pm: "م"
                )
                char: #(
                    am: "ص"
                    pm: "م"
                )
                full: #(
                    am: "ص"
                    pm: "م"
                )
            )
            eras: #(
                full: #(
                    BC: "قبل الميلاد"
                    AD: "ميلادي"
                    BCE: "قبل الحقبة الحالية"
                    CE: "بعد الميلاد"
                )
                abbr: #(
                    BC: "ق.م"
                    AD: "م"
                    BCE: "ق. م"
                    CE: "ب.م"
                )
            )
        )
        masks: #(
            date: #(
                full: "Sunday، 31 December 1999"
                long: "31 December 1999"
                medium: "031‏/012‏/1999"
                short: "31‏/12‏/1999"
            )
            time: #(
                full: "12:59:59 AM 'غرينتش'+00:00"
                long: "12:59:59 AM 'غرينتش'+0"
                medium: "12:59:59 AM"
                short: "12:59 AM"
            )
            datetime: #(
                full: {Sunday، 31 December 1999 في 12:59:59 AM 'غرينتش'+00:00}
                long: "31 December 1999 في 12:59:59 AM 'غرينتش'+0"
                medium: "031‏/012‏/1999, 12:59:59 AM"
                short: "31‏/12‏/1999, 12:59 AM"
            )
        )
    )
    currency-names: #(
        AED: [std: "د.إ.‏"]
        ARS: [char: "AR$"]
        AUD: [std: "AU$"]
        BBD: [char: "BB$"]
        BHD: [std: "د.ب.‏"]
        BMD: [char: "BM$"]
        BND: [char: "BN$"]
        BSD: [char: "BS$"]
        BYN: [char: "р."]
        BZD: [char: "BZ$"]
        CAD: [std: "CA$"]
        CLP: [char: "CL$"]
        CNY: [std: "CN¥"]
        COP: [char: "CO$"]
        CUP: [char: "CU$"]
        DOP: [char: "DO$"]
        DZD: [std: "د.ج.‏"]
        EGP: [char: "E£" std: "ج.م.‏"]
        FJD: [char: "FJ$"]
        GBP: [std: "UK£"]
        GYD: [char: "GY$"]
        HKD: [std: "HK$"]
        IQD: [std: "د.ع.‏"]
        IRR: [std: "ر.إ."]
        JMD: [char: "JM$"]
        JOD: [std: "د.أ.‏"]
        JPY: [std: "JP¥"]
        KWD: [std: "د.ك.‏"]
        KYD: [char: "KY$"]
        LBP: [char: "L£" std: "ل.ل.‏"]
        LRD: [char: "$LR"]
        LYD: [std: "د.ل.‏"]
        MAD: [std: "د.م.‏"]
        MRU: [std: "أ.م."]
        MXN: [std: "MX$"]
        NZD: [std: "NZ$"]
        OMR: [std: "ر.ع.‏"]
        PHP: [char: "₱"]
        QAR: [std: "ر.ق.‏"]
        SAR: [std: "ر.س.‏"]
        SBD: [char: "SB$"]
        SDD: [std: "د.س.‏"]
        SDG: [std: "ج.س."]
        SRD: [char: "SR$"]
        SYP: [char: "£" std: "ل.س.‏"]
        THB: [std: "฿"]
        TND: [std: "د.ت.‏"]
        TTD: [char: "TT$"]
        TWD: [std: "NT$"]
        USD: [std: "US$"]
        UYU: [char: "UY$"]
        YER: [std: "ر.ي.‏"]
    )
    parent: root
)
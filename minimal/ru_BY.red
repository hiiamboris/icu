Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/ru_BY: #(
    region-name: "Беларусь"
    currency: BYN
    currency-names: #(
        BYN: [std: "Br"]
    )
    parent: ru
)
# Red locale data

Files here with the exception of `red.red` were *generated from* CLDR data using [these tools](https://gitlab.com/hiiamboris/icu) and as such are subject to original [Unicode.org license](https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE)

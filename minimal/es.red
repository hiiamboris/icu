Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/es: #(
    lang-name: "español"
    region-name: none
    currency: EUR
    numbers: #(
        system: latn
        latn: #(
            digits: "0123456789"
            fin-digits: "0123456789"
            symbols: #(
                nan: "NaN"
                infinity: "∞"
                permille: "‰"
                superscripting-exponent: "×"
                exponential: "E"
                approximately: "~"
                minus: "-"
                plus: "+"
                percent: "%"
                list: ";"
                group: "."
                decimal: ","
            )
            masks: #(
                number: #(
                    dec: "# ##0.###"
                    sci: "0.##############E0"
                    pct: "# ##0. %"
                    eng: "0.##############E3"
                )
                money: #(
                    std: "# ##0.00 $$"
                    acct: "# ##0.00 $$"
                )
            )
        )
        ordinal-suffixes: #(
            other: ".ª"
        )
    )
    calendar: #(
        day1: mon
        standalone: #(
            months: #(
                abbr: ["ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sept" "oct" "nov" "dic"]
                char: ["E" "F" "M" "A" "M" "J" "J" "A" "S" "O" "N" "D"]
                full: ["enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre"]
            )
            days: #(
                abbr: #(
                    sun: "dom"
                    mon: "lun"
                    tue: "mar"
                    wed: "mié"
                    thu: "jue"
                    fri: "vie"
                    sat: "sáb"
                )
                char: #(
                    sun: "D"
                    mon: "L"
                    tue: "M"
                    wed: "X"
                    thu: "J"
                    fri: "V"
                )
                short: #(
                    sun: "DO"
                    mon: "LU"
                    tue: "MA"
                    wed: "MI"
                    thu: "JU"
                    fri: "VI"
                    sat: "SA"
                )
                full: #(
                    sun: "domingo"
                    mon: "lunes"
                    tue: "martes"
                    wed: "miércoles"
                    thu: "jueves"
                    fri: "viernes"
                    sat: "sábado"
                )
            )
            quarters: #(
                abbr: ["T1" "T2" "T3" "T4"]
                full: ["1.er trimestre" "2.º trimestre" "3.er trimestre" "4.º trimestre"]
            )
            periods: #(
                abbr: #(
                    am: "a. m."
                    pm: "p. m."
                )
                char: #(
                    am: "a. m."
                    pm: "p. m."
                )
                full: #(
                    am: "a. m."
                    pm: "p. m."
                )
            )
        )
        format: #(
            months: #(
                abbr: ["ene" "feb" "mar" "abr" "may" "jun" "jul" "ago" "sept" "oct" "nov" "dic"]
                char: ["E" "F" "M" "A" "M" "J" "J" "A" "S" "O" "N" "D"]
                full: ["enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre"]
            )
            days: #(
                abbr: #(
                    sun: "dom"
                    mon: "lun"
                    tue: "mar"
                    wed: "mié"
                    thu: "jue"
                    fri: "vie"
                    sat: "sáb"
                )
                char: #(
                    sun: "D"
                    mon: "L"
                    tue: "M"
                    wed: "X"
                    thu: "J"
                    fri: "V"
                )
                short: #(
                    sun: "DO"
                    mon: "LU"
                    tue: "MA"
                    wed: "MI"
                    thu: "JU"
                    fri: "VI"
                    sat: "SA"
                )
                full: #(
                    sun: "domingo"
                    mon: "lunes"
                    tue: "martes"
                    wed: "miércoles"
                    thu: "jueves"
                    fri: "viernes"
                    sat: "sábado"
                )
            )
            quarters: #(
                abbr: ["T1" "T2" "T3" "T4"]
                full: ["1.er trimestre" "2.º trimestre" "3.er trimestre" "4.º trimestre"]
            )
            periods: #(
                abbr: #(
                    am: "a. m."
                    pm: "p. m."
                )
                char: #(
                    am: "a. m."
                    pm: "p. m."
                )
                full: #(
                    am: "a. m."
                    pm: "p. m."
                )
            )
            eras: #(
                full: #(
                    BC: "antes de Cristo"
                    AD: "después de Cristo"
                    BCE: "antes de la era común"
                    CE: "era común"
                )
                abbr: #(
                    BC: "a. C."
                    AD: "d. C."
                    BCE: "a. e. c."
                    CE: "e. c."
                )
            )
        )
        masks: #(
            date: #(
                full: "Sunday, 31 'de' December 'de' 1999"
                long: "31 'de' December 'de' 1999"
                medium: "31 Dec 1999"
                short: "31/12/99"
            )
            time: #(
                full: "23:59:59 ('GMT'+00:00)"
                long: "23:59:59 'GMT'+0"
                medium: "23:59:59"
                short: "23:59"
            )
            datetime: #(
                full: {Sunday, 31 'de' December 'de' 1999, 23:59:59 ('GMT'+00:00)}
                long: "31 'de' December 'de' 1999, 23:59:59 'GMT'+0"
                medium: "31 Dec 1999, 23:59:59"
                short: "31/12/99, 23:59"
            )
        )
    )
    currency-names: #(
        AUD: [char: "$"]
        BRL: [char: "R$"]
        BYN: [char: "р."]
        CAD: [char: "$"]
        CNY: [char: "¥"]
        ESP: [std: "₧"]
        GBP: [char: "£"]
        HKD: [char: "$"]
        ILS: [char: "₪"]
        INR: [char: "₹"]
        JPY: [char: "¥"]
        KRW: [char: "₩"]
        MXN: [char: "$"]
        NZD: [char: "$"]
        PHP: [char: "₱"]
        RON: [char: "L"]
        THB: [std: "฿"]
        TWD: [char: "NT$"]
        XCD: [char: "$"]
    )
    parent: root
)
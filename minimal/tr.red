Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/tr: #(
    lang-name: "Türkçe"
    region-name: none
    currency: TRY
    numbers: #(
        system: latn
        latn: #(
            digits: "0123456789"
            fin-digits: "0123456789"
            symbols: #(
                nan: "NaN"
                infinity: "∞"
                permille: "‰"
                superscripting-exponent: "×"
                exponential: "E"
                approximately: "~"
                minus: "-"
                plus: "+"
                percent: "%"
                list: ";"
                group: "."
                decimal: ","
            )
            masks: #(
                number: #(
                    dec: "# ##0.###"
                    sci: "0.##############E0"
                    pct: "%# ##0."
                    eng: "0.##############E3"
                )
                money: #(
                    std: "$$# ##0.00"
                    acct: "($$# ##0.00)"
                )
            )
        )
        ordinal-suffixes: #(
            other: "."
        )
    )
    calendar: #(
        day1: mon
        standalone: #(
            months: #(
                abbr: ["Oca" "Şub" "Mar" "Nis" "May" "Haz" "Tem" "Ağu" "Eyl" "Eki" "Kas" "Ara"]
                char: ["O" "Ş" "M" "N" "M" "H" "T" "A" "E" "E" "K" "A"]
                full: ["Ocak" "Şubat" "Mart" "Nisan" "Mayıs" "Haziran" "Temmuz" "Ağustos" "Eylül" "Ekim" "Kasım" "Aralık"]
            )
            days: #(
                abbr: #(
                    sun: "Paz"
                    mon: "Pzt"
                    tue: "Sal"
                    wed: "Çar"
                    thu: "Per"
                    fri: "Cum"
                    sat: "Cmt"
                )
                char: #(
                    sun: "P"
                    mon: "P"
                    tue: "S"
                    wed: "Ç"
                    thu: "P"
                    fri: "C"
                    sat: "C"
                )
                short: #(
                    sun: "Pa"
                    mon: "Pt"
                    tue: "Sa"
                    wed: "Ça"
                    thu: "Pe"
                    fri: "Cu"
                    sat: "Ct"
                )
                full: #(
                    sun: "Pazar"
                    mon: "Pazartesi"
                    tue: "Salı"
                    wed: "Çarşamba"
                    thu: "Perşembe"
                    fri: "Cuma"
                    sat: "Cumartesi"
                )
            )
            quarters: #(
                abbr: ["Ç1" "Ç2" "Ç3" "Ç4"]
                char: ["1." "2." "3." "4."]
                full: ["1. çeyrek" "2. çeyrek" "3. çeyrek" "4. çeyrek"]
            )
            periods: #(
                abbr: #(
                    am: "ÖÖ"
                    pm: "ÖS"
                )
                char: #(
                    am: "ÖÖ"
                    pm: "ÖS"
                )
                full: #(
                    am: "ÖÖ"
                    pm: "ÖS"
                )
            )
        )
        format: #(
            months: #(
                abbr: ["Oca" "Şub" "Mar" "Nis" "May" "Haz" "Tem" "Ağu" "Eyl" "Eki" "Kas" "Ara"]
                char: ["O" "Ş" "M" "N" "M" "H" "T" "A" "E" "E" "K" "A"]
                full: ["Ocak" "Şubat" "Mart" "Nisan" "Mayıs" "Haziran" "Temmuz" "Ağustos" "Eylül" "Ekim" "Kasım" "Aralık"]
            )
            days: #(
                abbr: #(
                    sun: "Paz"
                    mon: "Pzt"
                    tue: "Sal"
                    wed: "Çar"
                    thu: "Per"
                    fri: "Cum"
                    sat: "Cmt"
                )
                char: #(
                    sun: "P"
                    mon: "P"
                    tue: "S"
                    wed: "Ç"
                    thu: "P"
                    fri: "C"
                    sat: "C"
                )
                short: #(
                    sun: "Pa"
                    mon: "Pt"
                    tue: "Sa"
                    wed: "Ça"
                    thu: "Pe"
                    fri: "Cu"
                    sat: "Ct"
                )
                full: #(
                    sun: "Pazar"
                    mon: "Pazartesi"
                    tue: "Salı"
                    wed: "Çarşamba"
                    thu: "Perşembe"
                    fri: "Cuma"
                    sat: "Cumartesi"
                )
            )
            quarters: #(
                abbr: ["Ç1" "Ç2" "Ç3" "Ç4"]
                char: ["1." "2." "3." "4."]
                full: ["1. çeyrek" "2. çeyrek" "3. çeyrek" "4. çeyrek"]
            )
            periods: #(
                abbr: #(
                    am: "ÖÖ"
                    pm: "ÖS"
                )
                char: #(
                    am: "öö"
                    pm: "ös"
                )
                full: #(
                    am: "ÖÖ"
                    pm: "ÖS"
                )
            )
            eras: #(
                full: #(
                    BC: "Milattan Önce"
                    AD: "Milattan Sonra"
                    BCE: "İsa’dan Önce"
                    CE: "İsa’dan Sonra"
                )
                abbr: #(
                    BC: "MÖ"
                    AD: "MS"
                    BCE: "İÖ"
                    CE: "İS"
                )
            )
        )
        masks: #(
            date: #(
                full: "31 December 1999 Sunday"
                long: "31 December 1999"
                medium: "31 Dec 1999"
                short: "31.012.1999"
            )
            datetime: #(
                full: "31 December 1999 Sunday 023:59:59 'GMT'+00:00"
                long: "31 December 1999 023:59:59 'GMT'+0"
                medium: "31 Dec 1999 023:59:59"
                short: "31.012.1999 023:59"
            )
        )
    )
    currency-names: #(
        AUD: [char: "$" std: "AU$"]
        BYN: [char: "р."]
        JPY: [std: "¥"]
        PHP: [char: "₱"]
        RON: [char: "L"]
        RUR: [char: "р."]
        THB: [std: "฿"]
        TRY: [std: "₺"]
        TWD: [std: "NT$"]
        USD: [std: "$"]
    )
    parent: root
)
Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/bg: #(
    lang-name: "български"
    region-name: none
    currency: BGN
    numbers: #(
        system: latn
        latn: #(
            digits: "0123456789"
            fin-digits: "0123456789"
            symbols: #(
                nan: "NaN"
                infinity: "∞"
                permille: "‰"
                superscripting-exponent: "×"
                exponential: "E"
                approximately: "~"
                minus: "-"
                plus: "+"
                percent: "%"
                list: ";"
                group: " "
                decimal: ","
            )
            masks: #(
                number: #(
                    dec: "# ##0.###"
                    sci: "0.##############E0"
                    pct: "# ##0.%"
                    eng: "0.##############E3"
                )
                money: #(
                    std: "0.00 $$"
                    acct: "(0.00 $$)"
                )
            )
        )
        ordinal-suffixes: #(
            other: "-ата"
        )
    )
    calendar: #(
        day1: mon
        standalone: #(
            months: #(
                abbr: ["яну" "фев" "март" "апр" "май" "юни" "юли" "авг" "сеп" "окт" "ное" "дек"]
                char: ["я" "ф" "м" "а" "м" "ю" "ю" "а" "с" "о" "н" "д"]
                full: ["януари" "февруари" "март" "април" "май" "юни" "юли" "август" "септември" "октомври" "ноември" "декември"]
            )
            days: #(
                abbr: #(
                    sun: "нд"
                    mon: "пн"
                    tue: "вт"
                    wed: "ср"
                    thu: "чт"
                    fri: "пт"
                    sat: "сб"
                )
                char: #(
                    sun: "н"
                    mon: "п"
                    tue: "в"
                    wed: "с"
                    thu: "ч"
                    fri: "п"
                    sat: "с"
                )
                short: #(
                    sun: "нд"
                    mon: "пн"
                    tue: "вт"
                    wed: "ср"
                    thu: "чт"
                    fri: "пт"
                    sat: "сб"
                )
                full: #(
                    sun: "неделя"
                    mon: "понеделник"
                    tue: "вторник"
                    wed: "сряда"
                    thu: "четвъртък"
                    fri: "петък"
                    sat: "събота"
                )
            )
            quarters: #(
                abbr: ["1. трим." "2. трим." "3. трим." "4. трим."]
                full: ["1. тримесечие" "2. тримесечие" "3. тримесечие" "4. тримесечие"]
            )
            periods: #(
                abbr: #(
                    am: "am"
                    pm: "pm"
                )
                char: #(
                    am: "am"
                    pm: "pm"
                )
                full: #(
                    am: "am"
                    pm: "pm"
                )
            )
        )
        format: #(
            months: #(
                abbr: ["яну" "фев" "март" "апр" "май" "юни" "юли" "авг" "сеп" "окт" "ное" "дек"]
                char: ["я" "ф" "м" "а" "м" "ю" "ю" "а" "с" "о" "н" "д"]
                full: ["януари" "февруари" "март" "април" "май" "юни" "юли" "август" "септември" "октомври" "ноември" "декември"]
            )
            days: #(
                abbr: #(
                    sun: "нд"
                    mon: "пн"
                    tue: "вт"
                    wed: "ср"
                    thu: "чт"
                    fri: "пт"
                    sat: "сб"
                )
                char: #(
                    sun: "н"
                    mon: "п"
                    tue: "в"
                    wed: "с"
                    thu: "ч"
                    fri: "п"
                    sat: "с"
                )
                short: #(
                    sun: "нд"
                    mon: "пн"
                    tue: "вт"
                    wed: "ср"
                    thu: "чт"
                    fri: "пт"
                    sat: "сб"
                )
                full: #(
                    sun: "неделя"
                    mon: "понеделник"
                    tue: "вторник"
                    wed: "сряда"
                    thu: "четвъртък"
                    fri: "петък"
                    sat: "събота"
                )
            )
            quarters: #(
                abbr: ["1. трим." "2. трим." "3. трим." "4. трим."]
                full: ["1. тримесечие" "2. тримесечие" "3. тримесечие" "4. тримесечие"]
            )
            periods: #(
                abbr: #(
                    am: "am"
                    pm: "pm"
                )
                char: #(
                    am: "am"
                    pm: "pm"
                )
                full: #(
                    am: "пр.об."
                    pm: "сл.об."
                )
            )
            eras: #(
                full: #(
                    BC: "преди Христа"
                    AD: "след Христа"
                    BCE: "преди новата ера"
                    CE: "след новата ера"
                )
                abbr: #(
                    BC: "пр.Хр."
                    AD: "сл.Хр."
                    BCE: "пр.н.е."
                    CE: "сл.н.е."
                )
            )
        )
        masks: #(
            date: #(
                full: "Sunday, 31 December 1999 'г'."
                long: "31 December 1999 'г'."
                medium: "31.012.1999 'г'."
                short: "31.012.99 'г'."
            )
            time: #(
                full: "23:59:59 'ч'. 'Гринуич'+00:00"
                long: "23:59:59 'ч'. 'Гринуич'+0"
                medium: "23:59:59 'ч'."
                short: "23:59 'ч'."
            )
            datetime: #(
                full: {Sunday, 31 December 1999 'г'., 23:59:59 'ч'. 'Гринуич'+00:00}
                long: "31 December 1999 'г'., 23:59:59 'ч'. 'Гринуич'+0"
                medium: "31.012.1999 'г'., 23:59:59 'ч'."
                short: "31.012.99 'г'., 23:59 'ч'."
            )
        )
    )
    currency-names: #(
        AFN: [char: "Af"]
        BGN: [std: "лв."]
        GBP: [char: "£"]
        JPY: [char: "¥"]
        USD: [char: "$" std: "щ.д."]
        XCD: [char: "$"]
    )
    parent: root
)
Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/fr_CA: #(
    region-name: "Canada"
    currency: CAD
    numbers: #(
        latn: #(
            symbols: #(
                approximately: "≈"
                group: " "
            )
        )
    )
    calendar: #(
        day1: sun
        standalone: #(
            months: #(
                abbr: ["janv." "févr." "mars" "avr." "mai" "juin" "juill." "août" "sept." "oct." "nov." "déc."]
            )
            periods: #(
                abbr: #(
                    am: "a.m."
                    pm: "p.m."
                )
                char: #(
                    am: "a.m."
                    pm: "p.m."
                )
                full: #(
                    am: "a.m."
                    pm: "p.m."
                )
            )
        )
        format: #(
            months: #(
                abbr: ["janv." "févr." "mars" "avr." "mai" "juin" "juill." "août" "sept." "oct." "nov." "déc."]
            )
            periods: #(
                abbr: #(
                    am: "a.m."
                    pm: "p.m."
                )
                char: #(
                    am: "a"
                    pm: "p"
                )
                full: #(
                    am: "a.m."
                    pm: "p.m."
                )
            )
            eras: #(
                full: #(
                    BCE: "avant l’ère chrétienne"
                    CE: "de l’ère chrétienne"
                )
            )
        )
        masks: #(
            date: #(
                short: "1999-012-031"
            )
            time: #(
                full: "023 'h' 59 'min' 59 's' 'UTC'+00:00"
                long: "023 'h' 59 'min' 59 's' 'UTC'+0"
                medium: "023 'h' 59 'min' 59 's'"
                short: "023 'h' 59"
            )
            datetime: #(
                full: {Sunday 31 December 1999 'à' 023 'h' 59 'min' 59 's' 'UTC'+00:00}
                long: {31 December 1999 'à' 023 'h' 59 'min' 59 's' 'UTC'+0}
                medium: "31 Dec 1999, 023 'h' 59 'min' 59 's'"
                short: "1999-012-031 023 'h' 59"
            )
        )
    )
    currency-names: #(
        ARS: [char: "$"]
        AUD: [char: "$" std: "$ AU"]
        BMD: [char: "$"]
        BND: [char: "$"]
        BYN: [char: "Br"]
        BZD: [char: "$"]
        CAD: [std: "$"]
        CLP: [char: "$"]
        CNY: [char: "¥" std: "CN¥"]
        COP: [char: "$"]
        FJD: [char: "$"]
        FKP: [char: "£"]
        GBP: [std: "£"]
        GIP: [char: "£"]
        HKD: [char: "$" std: "$ HK"]
        ILS: [char: "₪"]
        INR: [char: "₹"]
        JPY: [std: "¥"]
        KMF: [char: "CF"]
        KRW: [char: "₩"]
        LBP: [char: "£L"]
        MXN: [char: "$"]
        NAD: [char: "$"]
        NIO: [char: "C$"]
        NZD: [char: "$" std: "$ NZ"]
        SBD: [char: "$"]
        SGD: [char: "$" std: "$ SG"]
        SRD: [char: "$"]
        TTD: [char: "$"]
        USD: [char: "$" std: "$ US"]
        UYU: [char: "$"]
        VND: [char: "₫"]
        ZMW: [char: "ZK"]
    )
    parent: fr
)
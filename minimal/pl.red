Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/pl: #(
    lang-name: "polski"
    region-name: none
    currency: PLN
    numbers: #(
        system: latn
        latn: #(
            digits: "0123456789"
            fin-digits: "0123456789"
            symbols: #(
                nan: "NaN"
                infinity: "∞"
                permille: "‰"
                superscripting-exponent: "×"
                exponential: "E"
                approximately: "~"
                minus: "-"
                plus: "+"
                percent: "%"
                list: ";"
                group: " "
                decimal: ","
            )
            masks: #(
                number: #(
                    dec: "# ##0.###"
                    sci: "0.##############E0"
                    pct: "# ##0.%"
                    eng: "0.##############E3"
                )
                money: #(
                    std: "# ##0.00 $$"
                    acct: "(# ##0.00 $$)"
                )
            )
        )
        ordinal-suffixes: #(
            other: "."
        )
    )
    calendar: #(
        day1: mon
        standalone: #(
            months: #(
                abbr: ["sty" "lut" "mar" "kwi" "maj" "cze" "lip" "sie" "wrz" "paź" "lis" "gru"]
                char: ["S" "L" "M" "K" "M" "C" "L" "S" "W" "P" "L" "G"]
                full: ["styczeń" "luty" "marzec" "kwiecień" "maj" "czerwiec" "lipiec" "sierpień" "wrzesień" "październik" "listopad" "grudzień"]
            )
            days: #(
                abbr: #(
                    sun: "niedz."
                    mon: "pon."
                    tue: "wt."
                    wed: "śr."
                    thu: "czw."
                    fri: "pt."
                    sat: "sob."
                )
                char: #(
                    sun: "N"
                    mon: "P"
                    tue: "W"
                    wed: "Ś"
                    thu: "C"
                    fri: "P"
                )
                short: #(
                    sun: "nie"
                    mon: "pon"
                    tue: "wto"
                    wed: "śro"
                    thu: "czw"
                    fri: "pią"
                    sat: "sob"
                )
                full: #(
                    sun: "niedziela"
                    mon: "poniedziałek"
                    tue: "wtorek"
                    wed: "środa"
                    thu: "czwartek"
                    fri: "piątek"
                    sat: "sobota"
                )
            )
            quarters: #(
                abbr: ["I kw." "II kw." "III kw." "IV kw."]
                full: ["I kwartał" "II kwartał" "III kwartał" "IV kwartał"]
            )
            periods: #(
                char: #(
                    am: "a"
                    pm: "p"
                )
            )
        )
        format: #(
            months: #(
                abbr: ["sty" "lut" "mar" "kwi" "maj" "cze" "lip" "sie" "wrz" "paź" "lis" "gru"]
                char: ["s" "l" "m" "k" "m" "c" "l" "s" "w" "p" "l" "g"]
                full: ["stycznia" "lutego" "marca" "kwietnia" "maja" "czerwca" "lipca" "sierpnia" "września" "października" "listopada" "grudnia"]
            )
            days: #(
                abbr: #(
                    sun: "niedz."
                    mon: "pon."
                    tue: "wt."
                    wed: "śr."
                    thu: "czw."
                    fri: "pt."
                    sat: "sob."
                )
                char: #(
                    sun: "n"
                    mon: "p"
                    tue: "w"
                    wed: "ś"
                    thu: "c"
                    fri: "p"
                    sat: "s"
                )
                short: #(
                    sun: "nie"
                    mon: "pon"
                    tue: "wto"
                    wed: "śro"
                    thu: "czw"
                    fri: "pią"
                    sat: "sob"
                )
                full: #(
                    sun: "niedziela"
                    mon: "poniedziałek"
                    tue: "wtorek"
                    wed: "środa"
                    thu: "czwartek"
                    fri: "piątek"
                    sat: "sobota"
                )
            )
            quarters: #(
                abbr: ["I kw." "II kw." "III kw." "IV kw."]
                full: ["I kwartał" "II kwartał" "III kwartał" "IV kwartał"]
            )
            periods: #(
                char: #(
                    am: "a"
                    pm: "p"
                )
            )
            eras: #(
                full: #(
                    BC: "przed naszą erą"
                    AD: "naszej ery"
                    BCE: "p.n.e."
                    CE: "n.e."
                )
                abbr: #(
                    BC: "p.n.e."
                    AD: "n.e."
                    BCE: "BCE"
                    CE: "CE"
                )
            )
        )
        masks: #(
            date: #(
                full: "Sunday, 31 December 1999"
                long: "31 December 1999"
                medium: "31 Dec 1999"
                short: "31.012.1999"
            )
            datetime: #(
                full: "Sunday, 31 December 1999 023:59:59 'GMT'+00:00"
                long: "31 December 1999 023:59:59 'GMT'+0"
                medium: "31 Dec 1999, 023:59:59"
                short: "31.012.1999, 023:59"
            )
        )
    )
    currency-names: #(
        AUD: [char: "$"]
        CAD: [char: "$"]
        CNY: [char: "¥"]
        GBP: [char: "£"]
        HKD: [char: "$"]
        ILS: [char: "₪"]
        INR: [char: "₹"]
        JPY: [char: "¥"]
        KRW: [char: "₩"]
        MXN: [char: "$"]
        NZD: [char: "$"]
        PHP: [char: "₱"]
        PLN: [std: "zł"]
        RON: [char: "lej"]
        TWD: [char: "NT$"]
        USD: [char: "$"]
        VND: [char: "₫"]
    )
    parent: root
)
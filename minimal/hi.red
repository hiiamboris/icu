Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/hi: #(
    lang-name: "हिन्दी"
    region-name: none
    currency: INR
    numbers: #(
        system: latn
        latn: #(
            digits: "0123456789"
            fin-digits: "0123456789"
            symbols: #(
                nan: "NaN"
                infinity: "∞"
                permille: "‰"
                superscripting-exponent: "×"
                exponential: "E"
                approximately: "~"
                minus: "-"
                plus: "+"
                percent: "%"
                list: ";"
                group: ","
                decimal: "."
            )
            masks: #(
                number: #(
                    dec: "# ## ##0.###"
                    sci: "[0.E0]"
                    pct: "# ## ##0.%"
                    eng: "[0.E3]"
                )
                money: #(
                    std: "$$# ## ##0.00"
                    acct: "$$# ## ##0.00"
                )
            )
        )
        ordinal-suffixes: #(
            few: " दाहिना"
            many: "ठा"
            one: "ला"
            other: "वां"
            two: "रा"
        )
    )
    calendar: #(
        standalone: #(
            months: #(
                abbr: ["जन॰" "फ़र॰" "मार्च" "अप्रैल" "मई" "जून" "जुल॰" "अग॰" "सित॰" "अक्तू॰" "नव॰" "दिस॰"]
                char: ["ज" "फ़" "मा" "अ" "म" "जू" "जु" "अ" "सि" "अ" "न" "दि"]
                full: ["जनवरी" "फ़रवरी" "मार्च" "अप्रैल" "मई" "जून" "जुलाई" "अगस्त" "सितंबर" "अक्तूबर" "नवंबर" "दिसंबर"]
            )
            days: #(
                abbr: #(
                    sun: "रवि"
                    mon: "सोम"
                    tue: "मंगल"
                    wed: "बुध"
                    thu: "गुरु"
                    fri: "शुक्र"
                    sat: "शनि"
                )
                char: #(
                    sun: "र"
                    mon: "सो"
                    tue: "मं"
                    wed: "बु"
                    thu: "गु"
                    fri: "शु"
                    sat: "श"
                )
                short: #(
                    sun: "र"
                    mon: "सो"
                    tue: "मं"
                    wed: "बु"
                    thu: "गु"
                    fri: "शु"
                    sat: "श"
                )
                full: #(
                    sun: "रविवार"
                    mon: "सोमवार"
                    tue: "मंगलवार"
                    wed: "बुधवार"
                    thu: "गुरुवार"
                    fri: "शुक्रवार"
                    sat: "शनिवार"
                )
            )
            quarters: #(
                abbr: ["ति1" "ति2" "ति3" "ति4"]
                full: ["पहली तिमाही" "दूसरी तिमाही" "तीसरी तिमाही" "चौथी तिमाही"]
            )
            periods: #(
                abbr: #(
                    am: "am"
                    pm: "pm"
                )
                char: #(
                    am: "am"
                    pm: "pm"
                )
                full: #(
                    am: "am"
                    pm: "pm"
                )
            )
        )
        format: #(
            months: #(
                abbr: ["जन॰" "फ़र॰" "मार्च" "अप्रैल" "मई" "जून" "जुल॰" "अग॰" "सित॰" "अक्तू॰" "नव॰" "दिस॰"]
                char: ["ज" "फ़" "मा" "अ" "म" "जू" "जु" "अ" "सि" "अ" "न" "दि"]
                full: ["जनवरी" "फ़रवरी" "मार्च" "अप्रैल" "मई" "जून" "जुलाई" "अगस्त" "सितंबर" "अक्तूबर" "नवंबर" "दिसंबर"]
            )
            days: #(
                abbr: #(
                    sun: "रवि"
                    mon: "सोम"
                    tue: "मंगल"
                    wed: "बुध"
                    thu: "गुरु"
                    fri: "शुक्र"
                    sat: "शनि"
                )
                char: #(
                    sun: "र"
                    mon: "सो"
                    tue: "मं"
                    wed: "बु"
                    thu: "गु"
                    fri: "शु"
                    sat: "श"
                )
                short: #(
                    sun: "र"
                    mon: "सो"
                    tue: "मं"
                    wed: "बु"
                    thu: "गु"
                    fri: "शु"
                    sat: "श"
                )
                full: #(
                    sun: "रविवार"
                    mon: "सोमवार"
                    tue: "मंगलवार"
                    wed: "बुधवार"
                    thu: "गुरुवार"
                    fri: "शुक्रवार"
                    sat: "शनिवार"
                )
            )
            quarters: #(
                abbr: ["ति1" "ति2" "ति3" "ति4"]
                full: ["पहली तिमाही" "दूसरी तिमाही" "तीसरी तिमाही" "चौथी तिमाही"]
            )
            periods: #(
                abbr: #(
                    am: "am"
                    pm: "pm"
                )
                char: #(
                    am: "am"
                    pm: "pm"
                )
                full: #(
                    am: "am"
                    pm: "pm"
                )
            )
            eras: #(
                full: #(
                    BC: "ईसा-पूर्व"
                    AD: "ईसवी सन"
                    BCE: "ईसवी पूर्व"
                    CE: "ईसवी"
                )
                abbr: #(
                    BC: "ईसा-पूर्व"
                    AD: "ईस्वी"
                    BCE: "ईसवी पूर्व"
                    CE: "ईसवी"
                )
            )
        )
        masks: #(
            date: #(
                full: "Sunday, 31 December 1999"
                long: "31 December 1999"
                medium: "31 Dec 1999"
                short: "31/12/99"
            )
            time: #(
                full: "12:59:59 AM 'GMT'+00:00"
                long: "12:59:59 AM 'GMT'+0"
                medium: "12:59:59 AM"
                short: "12:59 AM"
            )
            datetime: #(
                full: {Sunday, 31 December 1999 को 12:59:59 AM 'GMT'+00:00}
                long: "31 December 1999 को 12:59:59 AM 'GMT'+0"
                medium: "31 Dec 1999, 12:59:59 AM"
                short: "31/12/99, 12:59 AM"
            )
        )
    )
    currency-names: #(
        BYN: [char: "р."]
        PHP: [char: "₱"]
        RON: [char: "लेई"]
        THB: [std: "฿"]
        TWD: [std: "NT$"]
        USD: [std: "$"]
    )
    parent: root
)
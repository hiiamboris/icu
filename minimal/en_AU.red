Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/en_AU: #(
    region-name: "Australia"
    currency: AUD
    numbers: #(
        latn: #(
            symbols: #(
                exponential: "e"
            )
        )
    )
    calendar: #(
        day1: mon
        standalone: #(
            days: #(
                char: #(
                    sun: "Su."
                    mon: "M."
                    tue: "Tu."
                    wed: "W."
                    thu: "Th."
                    fri: "F."
                    sat: "Sa."
                )
                short: #(
                    mon: "Mon"
                    wed: "Wed"
                    fri: "Fri"
                    sat: "Sat"
                )
            )
            periods: #(
                char: #(
                    am: "am"
                    pm: "pm"
                )
            )
        )
        format: #(
            months: #(
                abbr: ["Jan" "Feb" "Mar" "Apr" "May" "June" "July" "Aug" "Sept" "Oct" "Nov" "Dec"]
            )
            days: #(
                char: #(
                    sun: "Su."
                    mon: "M."
                    tue: "Tu."
                    wed: "W."
                    thu: "Th."
                    fri: "F."
                    sat: "Sa."
                )
                short: #(
                    mon: "Mon"
                    wed: "Wed"
                    fri: "Fri"
                    sat: "Sat"
                )
            )
            periods: #(
                char: #(
                    am: "am"
                    pm: "pm"
                )
            )
        )
        masks: #(
            date: #(
                full: "Sunday, 31 December 1999"
                long: "31 December 1999"
                medium: "31 Dec 1999"
                short: "31/12/99"
            )
            datetime: #(
                full: {Sunday, 31 December 1999 'at' 12:59:59 AM 'GMT'+00:00}
                long: "31 December 1999 'at' 12:59:59 AM 'GMT'+0"
                medium: "31 Dec 1999, 12:59:59 AM"
                short: "31/12/99, 12:59 AM"
            )
        )
    )
    currency-names: #(
        AUD: [std: "$"]
        BDT: [char: "Tk"]
        BRL: [char: "R$"]
        CAD: [char: "$"]
        CNY: [char: "¥"]
        CUP: [char: "₱"]
        EGP: [char: "£"]
        EUR: [char: "€"]
        GBP: [char: "£"]
        HKD: [char: "$"]
        ILS: [char: "₪"]
        INR: [char: "₹"]
        ISK: [char: "Kr"]
        JPY: [char: "¥"]
        KRW: [char: "₩"]
        MXN: [char: "$"]
        NZD: [char: "$"]
        PHP: [char: "₱"]
        PYG: [char: "Gs"]
        SCR: [std: "Rs"]
        SEK: [char: "Kr"]
        TWD: [char: "$"]
        USD: [char: "$"]
        UYU: [char: "$U"]
        VND: [char: "₫"]
        XCD: [char: "$"]
        XPF: [std: "CFP"]
    )
    parent: en
)
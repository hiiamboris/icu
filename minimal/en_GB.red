Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/en_GB: #(
    region-name: "United Kingdom"
    currency: GBP
    calendar: #(
        day1: mon
        standalone: #(
            months: #(
                abbr: ["Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sept" "Oct" "Nov" "Dec"]
            )
        )
        format: #(
            months: #(
                abbr: ["Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sept" "Oct" "Nov" "Dec"]
            )
        )
        masks: #(
            date: #(
                full: "Sunday, 31 December 1999"
                long: "31 December 1999"
                medium: "31 Dec 1999"
                short: "031/012/1999"
            )
            time: #(
                full: "023:59:59 'GMT'+00:00"
                long: "023:59:59 'GMT'+0"
                medium: "023:59:59"
                short: "023:59"
            )
            datetime: #(
                full: {Sunday, 31 December 1999 'at' 023:59:59 'GMT'+00:00}
                long: "31 December 1999 'at' 023:59:59 'GMT'+0"
                medium: "31 Dec 1999, 023:59:59"
                short: "031/012/1999, 023:59"
            )
        )
    )
    parent: en
)
Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/he: #(
    lang-name: "עברית"
    region-name: none
    currency: ILS
    numbers: #(
        system: latn
        latn: #(
            digits: "0123456789"
            fin-digits: "0123456789"
            symbols: #(
                nan: "NaN"
                infinity: "∞"
                permille: "‰"
                superscripting-exponent: "×"
                exponential: "E"
                approximately: "~"
                minus: "‎-"
                plus: "‎+"
                percent: "%"
                list: ";"
                group: ","
                decimal: "."
            )
            masks: #(
                number: #(
                    dec: "# ##0.###"
                    sci: "0.##############E0"
                    pct: "# ##0.%"
                    eng: "0.##############E3"
                )
                money: #(
                    std: "‏-# ##0.00 $$"
                    acct: "# ##0.00 $$"
                )
            )
        )
        ordinal-suffixes: #(
            other: ""
        )
    )
    calendar: #(
        standalone: #(
            months: #(
                abbr: ["ינו׳" "פבר׳" "מרץ" "אפר׳" "מאי" "יוני" "יולי" "אוג׳" "ספט׳" "אוק׳" "נוב׳" "דצמ׳"]
                full: ["ינואר" "פברואר" "מרץ" "אפריל" "מאי" "יוני" "יולי" "אוגוסט" "ספטמבר" "אוקטובר" "נובמבר" "דצמבר"]
            )
            days: #(
                abbr: #(
                    sun: "יום א׳"
                    mon: "יום ב׳"
                    tue: "יום ג׳"
                    wed: "יום ד׳"
                    thu: "יום ה׳"
                    fri: "יום ו׳"
                    sat: "שבת"
                )
                char: #(
                    sun: "א׳"
                    mon: "ב׳"
                    tue: "ג׳"
                    wed: "ד׳"
                    thu: "ה׳"
                    fri: "ו׳"
                    sat: "ש׳"
                )
                short: #(
                    sun: "א׳"
                    mon: "ב׳"
                    tue: "ג׳"
                    wed: "ד׳"
                    thu: "ה׳"
                    fri: "ו׳"
                    sat: "ש׳"
                )
                full: #(
                    sun: "יום ראשון"
                    mon: "יום שני"
                    tue: "יום שלישי"
                    wed: "יום רביעי"
                    thu: "יום חמישי"
                    fri: "יום שישי"
                    sat: "יום שבת"
                )
            )
            quarters: #(
                full: ["רבעון 1" "רבעון 2" "רבעון 3" "רבעון 4"]
            )
            periods: #(
                char: #(
                    am: "לפנה״צ"
                    pm: "אחה״צ"
                )
            )
        )
        format: #(
            months: #(
                abbr: ["ינו׳" "פבר׳" "מרץ" "אפר׳" "מאי" "יוני" "יולי" "אוג׳" "ספט׳" "אוק׳" "נוב׳" "דצמ׳"]
                full: ["ינואר" "פברואר" "מרץ" "אפריל" "מאי" "יוני" "יולי" "אוגוסט" "ספטמבר" "אוקטובר" "נובמבר" "דצמבר"]
            )
            days: #(
                abbr: #(
                    sun: "יום א׳"
                    mon: "יום ב׳"
                    tue: "יום ג׳"
                    wed: "יום ד׳"
                    thu: "יום ה׳"
                    fri: "יום ו׳"
                    sat: "שבת"
                )
                char: #(
                    sun: "א׳"
                    mon: "ב׳"
                    tue: "ג׳"
                    wed: "ד׳"
                    thu: "ה׳"
                    fri: "ו׳"
                    sat: "ש׳"
                )
                short: #(
                    sun: "א׳"
                    mon: "ב׳"
                    tue: "ג׳"
                    wed: "ד׳"
                    thu: "ה׳"
                    fri: "ו׳"
                    sat: "ש׳"
                )
                full: #(
                    sun: "יום ראשון"
                    mon: "יום שני"
                    tue: "יום שלישי"
                    wed: "יום רביעי"
                    thu: "יום חמישי"
                    fri: "יום שישי"
                    sat: "יום שבת"
                )
            )
            quarters: #(
                full: ["רבעון 1" "רבעון 2" "רבעון 3" "רבעון 4"]
            )
            periods: #(
                abbr: #(
                    am: "לפנה״צ"
                    pm: "אחה״צ"
                )
                char: #(
                    am: "לפנה״צ"
                    pm: "אחה״צ"
                )
                full: #(
                    am: "לפנה״צ"
                    pm: "אחה״צ"
                )
            )
            eras: #(
                full: #(
                    BC: "לפני הספירה"
                    AD: "לספירה"
                    BCE: "לפנה״ס"
                    CE: "CE"
                )
                abbr: #(
                    BC: "לפנה״ס"
                    AD: "לספירה"
                    BCE: "BCE"
                    CE: "CE"
                )
                char: #(
                    BC: "לפני"
                    AD: "אחריי"
                )
            )
        )
        masks: #(
            date: #(
                full: "Sunday, 31 בDecember 1999"
                long: "31 בDecember 1999"
                medium: "31 בDec 1999"
                short: "31.12.1999"
            )
            time: #(
                full: "23:59:59 'GMT'+00:00'‎'"
                long: "23:59:59 'GMT'+0'‎'"
                medium: "23:59:59"
                short: "23:59"
            )
            datetime: #(
                full: {Sunday, 31 בDecember 1999 בשעה 23:59:59 'GMT'+00:00'‎'}
                long: "31 בDecember 1999 בשעה 23:59:59 'GMT'+0'‎'"
                medium: "31 בDec 1999, 23:59:59"
                short: "31.12.1999, 23:59"
            )
        )
    )
    currency-names: #(
        BYN: [char: "р"]
        CNY: [char: "¥" std: "‎CN¥‎"]
        ILP: [std: "ל״י"]
        JPY: [std: "¥"]
        PHP: [char: "₱"]
        THB: [std: "฿"]
        TWD: [std: "NT$"]
        USD: [std: "$"]
    )
    parent: root
)
Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/fr: #(
    lang-name: "français"
    region-name: none
    currency: EUR
    numbers: #(
        system: latn
        latn: #(
            digits: "0123456789"
            fin-digits: "0123456789"
            symbols: #(
                nan: "NaN"
                infinity: "∞"
                permille: "‰"
                superscripting-exponent: "×"
                exponential: "E"
                approximately: "≃"
                minus: "-"
                plus: "+"
                percent: "%"
                list: ";"
                group: " "
                decimal: ","
            )
            masks: #(
                number: #(
                    dec: "# ##0.###"
                    sci: "0.##############E0"
                    pct: "# ##0. %"
                    eng: "0.##############E3"
                )
                money: #(
                    std: "# ##0.00 $$"
                    acct: "(# ##0.00 $$)"
                )
            )
        )
        ordinal-suffixes: #(
            one: "re"
            other: "e"
        )
    )
    calendar: #(
        day1: mon
        standalone: #(
            months: #(
                abbr: ["janv." "févr." "mars" "avr." "mai" "juin" "juil." "août" "sept." "oct." "nov." "déc."]
                char: ["J" "F" "M" "A" "M" "J" "J" "A" "S" "O" "N" "D"]
                full: ["janvier" "février" "mars" "avril" "mai" "juin" "juillet" "août" "septembre" "octobre" "novembre" "décembre"]
            )
            days: #(
                abbr: #(
                    sun: "dim."
                    mon: "lun."
                    tue: "mar."
                    wed: "mer."
                    thu: "jeu."
                    fri: "ven."
                    sat: "sam."
                )
                char: #(
                    sun: "D"
                    mon: "L"
                    tue: "M"
                    wed: "M"
                    thu: "J"
                    fri: "V"
                )
                short: #(
                    sun: "di"
                    mon: "lu"
                    tue: "ma"
                    wed: "me"
                    thu: "je"
                    fri: "ve"
                    sat: "sa"
                )
                full: #(
                    sun: "dimanche"
                    mon: "lundi"
                    tue: "mardi"
                    wed: "mercredi"
                    thu: "jeudi"
                    fri: "vendredi"
                    sat: "samedi"
                )
            )
            quarters: #(
                abbr: ["T1" "T2" "T3" "T4"]
                full: ["1er trimestre" "2e trimestre" "3e trimestre" "4e trimestre"]
            )
        )
        format: #(
            months: #(
                abbr: ["janv." "févr." "mars" "avr." "mai" "juin" "juil." "août" "sept." "oct." "nov." "déc."]
                char: ["J" "F" "M" "A" "M" "J" "J" "A" "S" "O" "N" "D"]
                full: ["janvier" "février" "mars" "avril" "mai" "juin" "juillet" "août" "septembre" "octobre" "novembre" "décembre"]
            )
            days: #(
                abbr: #(
                    sun: "dim."
                    mon: "lun."
                    tue: "mar."
                    wed: "mer."
                    thu: "jeu."
                    fri: "ven."
                    sat: "sam."
                )
                char: #(
                    sun: "D"
                    mon: "L"
                    tue: "M"
                    wed: "M"
                    thu: "J"
                    fri: "V"
                )
                short: #(
                    sun: "di"
                    mon: "lu"
                    tue: "ma"
                    wed: "me"
                    thu: "je"
                    fri: "ve"
                    sat: "sa"
                )
                full: #(
                    sun: "dimanche"
                    mon: "lundi"
                    tue: "mardi"
                    wed: "mercredi"
                    thu: "jeudi"
                    fri: "vendredi"
                    sat: "samedi"
                )
            )
            quarters: #(
                abbr: ["T1" "T2" "T3" "T4"]
                full: ["1er trimestre" "2e trimestre" "3e trimestre" "4e trimestre"]
            )
            eras: #(
                full: #(
                    BC: "avant Jésus-Christ"
                    AD: "après Jésus-Christ"
                    BCE: "avant l’ère commune"
                    CE: "de l’ère commune"
                )
                abbr: #(
                    BC: "av. J.-C."
                    AD: "ap. J.-C."
                    BCE: "AEC"
                    CE: "EC"
                )
                char: #(
                    BC: "av. J.-C."
                    AD: "ap. J.-C."
                    BCE: "AEC"
                    CE: "EC"
                )
            )
        )
        masks: #(
            date: #(
                full: "Sunday 31 December 1999"
                long: "31 December 1999"
                medium: "31 Dec 1999"
                short: "031/012/1999"
            )
            time: #(
                full: "023:59:59 'UTC'+00:00"
                long: "023:59:59 'UTC'+0"
            )
            datetime: #(
                full: "Sunday 31 December 1999 'à' 023:59:59 'UTC'+00:00"
                long: "31 December 1999 'à' 023:59:59 'UTC'+0"
                medium: "31 Dec 1999, 023:59:59"
                short: "031/012/1999 023:59"
            )
        )
    )
    currency-names: #(
        ARS: [char: "$" std: "$AR"]
        AUD: [char: "$" std: "$AU"]
        BEF: [std: "FB"]
        BMD: [char: "$" std: "$BM"]
        BND: [char: "$" std: "$BN"]
        BYN: [char: "р."]
        BZD: [char: "$" std: "$BZ"]
        CAD: [char: "$" std: "$CA"]
        CLP: [char: "$" std: "$CL"]
        CNY: [char: "¥"]
        COP: [char: "$" std: "$CO"]
        CYP: [std: "£CY"]
        EGP: [char: "£E"]
        FJD: [char: "$" std: "$FJ"]
        FKP: [char: "£" std: "£FK"]
        FRF: [std: "F"]
        GBP: [char: "£" std: "£GB"]
        GIP: [char: "£" std: "£GI"]
        HKD: [char: "$"]
        IEP: [std: "£IE"]
        ILP: [std: "£IL"]
        ITL: [std: "₤IT"]
        JPY: [char: "¥"]
        KMF: [char: "FC"]
        LBP: [char: "£L" std: "£LB"]
        MAF: [std: "fMA"]
        MGF: [std: "Fmg"]
        MTP: [std: "£MT"]
        MXN: [char: "$" std: "$MX"]
        NAD: [char: "$" std: "$NA"]
        NIO: [char: "$C"]
        NZD: [char: "$" std: "$NZ"]
        PHP: [char: "₱"]
        RHD: [std: "$RH"]
        RON: [char: "L"]
        RUR: [char: "р."]
        RWF: [char: "FR"]
        SBD: [char: "$" std: "$SB"]
        SGD: [char: "$" std: "$SG"]
        SRD: [char: "$" std: "$SR"]
        TOP: [char: "$T"]
        TTD: [char: "$" std: "$TT"]
        TWD: [char: "NT$"]
        USD: [char: "$" std: "$US"]
        UYU: [char: "$" std: "$UY"]
        WST: [std: "$WS"]
        XCD: [char: "$"]
        XDR: [std: "DTS"]
        XPF: [std: "FCFP"]
        ZMW: [char: "Kw"]
    )
    parent: root
)
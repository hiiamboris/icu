Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/af: #(
    lang-name: "Afrikaans"
    region-name: none
    currency: ZAR
    numbers: #(
        system: latn
        latn: #(
            digits: "0123456789"
            fin-digits: "0123456789"
            symbols: #(
                nan: "NaN"
                infinity: "∞"
                permille: "‰"
                superscripting-exponent: "×"
                exponential: "E"
                approximately: "~"
                minus: "-"
                plus: "+"
                percent: "%"
                list: ";"
                group: " "
                decimal: ","
            )
            masks: #(
                number: #(
                    dec: "# ##0.###"
                    sci: "0.##############E0"
                    pct: "# ##0.%"
                    eng: "0.##############E3"
                )
                money: #(
                    std: "$$# ##0.00"
                    acct: "($$# ##0.00)"
                )
            )
        )
        ordinal-suffixes: #(
            other: "e"
        )
    )
    calendar: #(
        standalone: #(
            months: #(
                abbr: ["Jan." "Feb." "Mrt." "Apr." "Mei" "Jun." "Jul." "Aug." "Sep." "Okt." "Nov." "Des."]
                char: ["J" "F" "M" "A" "M" "J" "J" "A" "S" "O" "N" "D"]
                full: ["Januarie" "Februarie" "Maart" "April" "Mei" "Junie" "Julie" "Augustus" "September" "Oktober" "November" "Desember"]
            )
            days: #(
                abbr: #(
                    sun: "So."
                    mon: "Ma."
                    tue: "Di."
                    wed: "Wo."
                    thu: "Do."
                    fri: "Vr."
                    sat: "Sa."
                )
                char: #(
                    tue: "D"
                    thu: "D"
                    fri: "V"
                )
                short: #(
                    sun: "So."
                    mon: "Ma."
                    tue: "Di."
                    wed: "Wo."
                    thu: "Do."
                    fri: "Vr."
                    sat: "Sa."
                )
                full: #(
                    sun: "Sondag"
                    mon: "Maandag"
                    tue: "Dinsdag"
                    wed: "Woensdag"
                    thu: "Donderdag"
                    fri: "Vrydag"
                    sat: "Saterdag"
                )
            )
            quarters: #(
                abbr: ["K1" "K2" "K3" "K4"]
                full: ["1ste kwartaal" "2de kwartaal" "3de kwartaal" "4de kwartaal"]
            )
            periods: #(
                abbr: #(
                    am: "vm."
                    pm: "nm."
                )
                char: #(
                    am: "v"
                    pm: "n"
                )
                full: #(
                    am: "vm."
                    pm: "nm."
                )
            )
        )
        format: #(
            months: #(
                abbr: ["Jan." "Feb." "Mrt." "Apr." "Mei" "Jun." "Jul." "Aug." "Sep." "Okt." "Nov." "Des."]
                char: ["J" "F" "M" "A" "M" "J" "J" "A" "S" "O" "N" "D"]
                full: ["Januarie" "Februarie" "Maart" "April" "Mei" "Junie" "Julie" "Augustus" "September" "Oktober" "November" "Desember"]
            )
            days: #(
                abbr: #(
                    sun: "So."
                    mon: "Ma."
                    tue: "Di."
                    wed: "Wo."
                    thu: "Do."
                    fri: "Vr."
                    sat: "Sa."
                )
                char: #(
                    tue: "D"
                    thu: "D"
                    fri: "V"
                )
                short: #(
                    sun: "So."
                    mon: "Ma."
                    tue: "Di."
                    wed: "Wo."
                    thu: "Do."
                    fri: "Vr."
                    sat: "Sa."
                )
                full: #(
                    sun: "Sondag"
                    mon: "Maandag"
                    tue: "Dinsdag"
                    wed: "Woensdag"
                    thu: "Donderdag"
                    fri: "Vrydag"
                    sat: "Saterdag"
                )
            )
            quarters: #(
                abbr: ["K1" "K2" "K3" "K4"]
                full: ["1ste kwartaal" "2de kwartaal" "3de kwartaal" "4de kwartaal"]
            )
            periods: #(
                abbr: #(
                    am: "vm."
                    pm: "nm."
                )
                char: #(
                    am: "v"
                    pm: "n"
                )
                full: #(
                    am: "vm."
                    pm: "nm."
                )
            )
            eras: #(
                full: #(
                    BC: "voor Christus"
                    AD: "na Christus"
                    BCE: "voor die gewone jaartelling"
                    CE: "gewone jaartelling"
                )
                abbr: #(
                    BC: "v.C."
                    AD: "n.C."
                    BCE: "v.g.j."
                    CE: "g.j."
                )
                char: #(
                    BC: "v.C."
                    AD: "n.C."
                    BCE: "vgj"
                    CE: "gj"
                )
            )
        )
        masks: #(
            date: #(
                full: "Sunday 031 December 1999"
                long: "031 December 1999"
                medium: "031 Dec 1999"
            )
            datetime: #(
                full: "Sunday 031 December 1999 023:59:59 'GMT'+00:00"
                long: "031 December 1999 023:59:59 'GMT'+0"
                medium: "031 Dec 1999 023:59:59"
            )
        )
    )
    currency-names: #(
        BYN: [char: "р."]
        CAD: [char: "$"]
        MXN: [char: "$"]
        PHP: [char: "₱"]
        RON: [char: "leu"]
        THB: [std: "฿"]
        TWD: [std: "NT$"]
        USD: [char: "$"]
        ZAR: [std: "R"]
    )
    parent: root
)
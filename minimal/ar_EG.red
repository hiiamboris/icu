Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/ar_EG: #(
    region-name: "مصر"
    currency: EGP
    calendar: #(
        day1: sat
    )
    parent: ar
)
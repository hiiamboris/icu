Red [
	title:  "Red locale data file"
	notes:  "DO NOT MODIFY! Generated automatically from CLDR data"
	license: https://github.com/unicode-cldr/cldr-core/blob/master/LICENSE
]

system/locale/list/en_CA: #(
    region-name: "Canada"
    currency: CAD
    calendar: #(
        standalone: #(
            months: #(
                abbr: ["Sept"]
            )
            periods: #(
                abbr: #(
                    am: "a.m."
                    pm: "p.m."
                )
                char: #(
                    am: "a.m."
                    pm: "pm"
                )
                full: #(
                    am: "a.m."
                    pm: "p.m."
                )
            )
        )
        format: #(
            months: #(
                abbr: ["Sept"]
            )
            periods: #(
                abbr: #(
                    am: "a.m."
                    pm: "p.m."
                )
                char: #(
                    am: "am"
                    pm: "pm"
                )
                full: #(
                    am: "a.m."
                    pm: "p.m."
                )
            )
        )
        masks: #(
            date: #(
                short: "1999-012-031"
            )
            datetime: #(
                short: "1999-012-031, 12:59 AM"
            )
        )
    )
    currency-names: #(
        CAD: [std: "$"]
        JPY: [char: "¥" std: "JP¥"]
        USD: [char: "$" std: "US$"]
    )
    parent: en
)